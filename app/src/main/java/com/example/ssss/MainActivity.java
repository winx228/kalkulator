package com.example.ssss;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1=findViewById(R.id.b1);
        textview=findViewById(R.id.t1);
        button2=findViewById(R.id.b2);
        button3=findViewById(R.id.b3);
        button4=findViewById(R.id.b4);
        button5=findViewById(R.id.b5);
        button6=findViewById(R.id.b6);
        button7=findViewById(R.id.b7);
        button8=findViewById(R.id.b8);
        button9=findViewById(R.id.b9);
        button10=findViewById(R.id.b10);
        button11=findViewById(R.id.b11);
        button12=findViewById(R.id.b12);
        button13=findViewById(R.id.b13);
        button14=findViewById(R.id.b14);
        button15=findViewById(R.id.b15);
        button16=findViewById(R.id.b16);
        button17=findViewById(R.id.b17);



    }
    Button button1;
    TextView textview;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button10;
    Button button11;
    Button button12;
    Button button13;
    Button button14;
    Button button15;
    Button button16;
    Button button17;

    Double i;
    String znak;

    public void click1(View view) {
        if (textview.getText().equals("0"))
        {
            textview.setText("1");
        }
        else
        {
            textview.setText(textview.getText()+"1");
        }
    }

    public void click2(View view) {
    }

    public void click3(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("2");
        }
        else
        {
            textview.setText(textview.getText()+"2");
        }
    }

    public void click4(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("3");
        }
        else
        {
            textview.setText(textview.getText()+"3");
        }
    }

    public void click5(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("4");
        }
        else
        {
            textview.setText(textview.getText()+"4");
        }
    }

    public void click6(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("5");
        }
        else
        {
            textview.setText(textview.getText()+"5");
        }
    }

    public void click7(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("6");
        }
        else
        {
            textview.setText(textview.getText()+"6");
        }
    }

    public void click8(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("7");
        }
        else
        {
            textview.setText(textview.getText()+"7");
        }
    }

    public void click9(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("8");
        }
        else
        {
            textview.setText(textview.getText()+"8");
        }
    }

    public void click10(View view) {
        i=Double.parseDouble(textview.getText().toString());
        textview.setText("0");
        znak = "-";
    }

    public void click0(View view) {
        if (textview.getText().toString().contains("."))
        {
            textview.setText(textview.getText()+"");
        }
        else
        {
            textview.setText(textview.getText()+".");
        }
    }

    public void clickC(View view) {
        textview.setText("0");
    }

    public void click00(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("0");
        }
        else
        {
            textview.setText(textview.getText()+"0");
        }
    }

    public void click99(View view) {
        if(textview.getText().equals("0"))
        {
            textview.setText("9");
        }
        else
        {
            textview.setText(textview.getText()+"9");
        }
    }

    public void clickdel(View view) {
        i=Double.parseDouble(textview.getText().toString());
        textview.setText("0");
        znak = "/";
    }

    public void clickRez(View view) {
        switch (znak)
        {
            case "/": textview.setText(String.valueOf(i / Double.parseDouble(textview.getText().toString())));
            break;
            case "+": textview.setText(String.valueOf(i + Double.parseDouble(textview.getText().toString())));
            break;
            case "-": textview.setText(String.valueOf(i - Double.parseDouble(textview.getText().toString())));
            break;
            case "*": textview.setText(String.valueOf(i * Double.parseDouble(textview.getText().toString())));
            break;
        }
    }

    public void clickplus(View view) {
        i=Double.parseDouble(textview.getText().toString());
        textview.setText("0");
        znak = "+";

    }

    public void clickYM(View view) {
        i=Double.parseDouble(textview.getText().toString());
        textview.setText("0");
        znak = "*";
    }
    private void getScreenOrientation() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            setTitle("Портретная ориентация");
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            setTitle("Альбомная ориентация");
    }
}
